# PHP POO Fake Project

This is a simple test project to try PHP, POO and database request.

## Database
You can import sql file to have example of data.

## ENV
Duplicate `.env.dist` file to create `.env` and update variables with yours.