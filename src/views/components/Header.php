<header>
  <nav class="h-20 m-10">
    <ul class="flex justify-between">
      <li><img class="w-8" src="./public/image/burger-bar.png" alt="burger-menu"></li>
      <li>
        <h1 class="font-black text-center text-4xl text-slate-800"><a href="index.php">NewsBlog</a></h1>
      </li>
      <li><a href="?page=create"><img class="w-8" src="./public/image/plus.png" alt="burger-menu"></a></li>
    </ul>
  </nav>
</header>